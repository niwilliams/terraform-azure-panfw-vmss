output "insights_keys" {
  description = "instrumentation key for app insight"
  value       = length(azurerm_application_insights.appinsight) == 1 ? azurerm_application_insights.appinsight[0].instrumentation_key : null
}
