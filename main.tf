/*
    Add the following line to the resource in this module that depends on the completion of external module components:

    depends_on = ["null_resource.module_depends_on"]

    This will force Terraform to wait until the dependant external resources are created before proceeding with the creation of the
    resource that contains the line above.

    This is a hack until Terraform officially support module depends_on.
*/

resource "null_resource" "module_depends_on" {
  triggers = {
    value = length(var.module_depends_on)
  }
}


resource "azurerm_marketplace_agreement" "pan_vmseries" {
  count     = var.subscribe ? 1 : 0
  publisher = "paloaltonetworks"
  offer     = "vmseries1"
  plan      = "byol"
}

resource "azurerm_application_insights" "appinsight" {
  count               = var.app_insights ? 1 : 0
  name                = "${var.scale_set_name}-appinsight"
  location            = var.location
  resource_group_name = var.resource_group_name
  application_type    = "web"
}

resource "azurerm_linux_virtual_machine_scale_set" "panfw_scale_set" {
  name                = var.scale_set_name
  location            = var.location
  resource_group_name = var.resource_group_name
  admin_username      = var.admin_username
  upgrade_mode        = "Manual"

  instances   = var.instance_count
  sku         = var.fw_size
  custom_data = var.bootstrap ? base64encode("storage-account=${var.bs_storage_account},access-key=${var.bs_access_key},file-share=${var.file_share},share-directory=") : null

  source_image_reference {
    publisher = var.fw_publisher
    offer     = var.fw_series
    sku       = var.fw_sku
    version   = var.fw_version
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  plan {
    name      = var.fw_sku
    product   = var.fw_series
    publisher = var.fw_publisher
  }

  admin_ssh_key {
    username   = var.admin_username
    public_key = var.ssh_key
  }

  computer_name_prefix = var.computer_name_prefix

  dynamic "network_interface" {
    for_each = { for interface in var.interfaces : interface.index => interface }
    content {
      name                          = network_interface.value.name
      primary                       = lookup(network_interface.value, "primary", null)
      enable_accelerated_networking = lookup(network_interface.value, "enable_accelerated_networking", null)
      enable_ip_forwarding          = lookup(network_interface.value, "enable_ip_forwarding", null)
      network_security_group_id     = lookup(network_interface.value, "network_security_group_id", null)
      ip_configuration {
        name                                   = "${network_interface.value.name}-ip"
        primary                                = true
        subnet_id                              = network_interface.value.subnet_id
        load_balancer_backend_address_pool_ids = lookup(network_interface.value, "load_balancer_backend_address_pool_ids", null)
        dynamic "public_ip_address" {
          for_each = contains(keys(network_interface.value), "pip_name") ? [{}] : []
          content {
            domain_name_label       = lookup(network_interface.value, "pip_domain_name_label", null)
            idle_timeout_in_minutes = "16"
            name                    = network_interface.value.pip_name
          }
        }
      }
    }
  }
  tags = var.tags
  lifecycle {
  ignore_changes = [
    instances,
    ]
  }
}

resource "azurerm_monitor_autoscale_setting" "settings" {
  name                = "${var.scale_set_name}-settings"
  resource_group_name = var.resource_group_name
  location            = var.location
  target_resource_id  = azurerm_linux_virtual_machine_scale_set.panfw_scale_set.id

  profile {
    name = "${var.scale_set_name}-profile"

    capacity {
      default = var.vmss_default_cap
      minimum = var.vmss_min_cap
      maximum = var.vmss_max_cap
    }

    dynamic "rule" {
      for_each = var.scale_set_rules
      content {
        metric_trigger {
          metric_name        = rule.key
          metric_resource_id = azurerm_linux_virtual_machine_scale_set.panfw_scale_set.id
          time_grain         = lookup(rule.value, "time_grain", "PT1M")
          statistic          = lookup(rule.value, "statistic", "Average")
          time_window        = lookup(rule.value, "time_window", "PT5M")
          time_aggregation   = lookup(rule.value, "time_aggregation", "Average")
          operator           = "GreaterThan"
          threshold          = rule.value.scale_out_threshold
        }

        scale_action {
          direction = "Increase"
          type      = lookup(rule.value, "type", "ChangeCount")
          value     = lookup(rule.value, "value", "1")
          cooldown  = lookup(rule.value, "cooldown", "PT1M")
        }
      }
    }

    dynamic "rule" {
      for_each = var.scale_set_rules
      content {
        metric_trigger {
          metric_name        = rule.key
          metric_resource_id = azurerm_linux_virtual_machine_scale_set.panfw_scale_set.id
          time_grain         = lookup(rule.value, "time_grain", "PT1M")
          statistic          = lookup(rule.value, "statistic", "Average")
          time_window        = lookup(rule.value, "time_window", "PT5M")
          time_aggregation   = lookup(rule.value, "time_aggregation", "Average")
          operator           = "LessThan"
          threshold          = rule.value.scale_in_threshold
        }

        scale_action {
          direction = "Decrease"
          type      = lookup(rule.value, "type", "ChangeCount")
          value     = lookup(rule.value, "value", "1")
          cooldown  = lookup(rule.value, "cooldown", "PT1M")
        }
      }
    }
  }
  notification {
    email {
      send_to_subscription_administrator    = true
      send_to_subscription_co_administrator = true
      custom_emails                         = var.email_notify
    }
  }
}
