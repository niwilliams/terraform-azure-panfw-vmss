variable "ssh_key" {}
variable "location" {}
variable "vnet_name" {}
variable "rg_name" {}
variable "subnet_name" {}
variable "computer_name_prefix" {}
variable "scale_set_name" {}
variable "email_notify" {}
